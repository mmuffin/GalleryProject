package com.maks.gallerytest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

public class PictureFragment extends Fragment {

    private static final String KEY_IMAGE_PATH = "key_image_path";

    public static PictureFragment newInstance(String imagePath) {
        PictureFragment fragment = new PictureFragment();
        Bundle args = new Bundle();
        args.putString(KEY_IMAGE_PATH, imagePath);
        fragment.setArguments(args);
        return fragment;
    }

    private ImageView picture;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_picture,container,false);

        picture = v.findViewById(R.id.picture_image_view);
        picture.setOnClickListener(v1 -> {
            ((PicturePagerActivity) getActivity()).changeAppBarVisibility();
        });
        if(getArguments() != null) {
            String imagePath = getArguments().getString(KEY_IMAGE_PATH);
            Glide.with(this)
                    .load(new File(imagePath))
                    .into(picture);
        }
        return v;
    }


}
