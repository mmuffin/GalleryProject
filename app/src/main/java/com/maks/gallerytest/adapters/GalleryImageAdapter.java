package com.maks.gallerytest.adapters;

import android.content.Context;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.maks.gallerytest.PicturePagerActivity;
import com.maks.gallerytest.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class GalleryImageAdapter extends RecyclerView.Adapter<GalleryImageAdapter.ImageViewHolder> {

    private ArrayList<File> images;
    private ArrayList<String> imagesPath;

    public GalleryImageAdapter(List<String> imagesPath) {
        this.imagesPath = new ArrayList<>();
        this.images = new ArrayList<>();

        this.imagesPath.addAll(imagesPath);
        for (String path : imagesPath) {
            this.images.add(new File(path));
        }
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_list_item, parent, false);
        return new ImageViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        holder.bindViewHolder(images.get(position), position);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void addItem(String filePath) {
        imagesPath.add(0, filePath);
        images.add(0, new File(filePath));
        notifyItemInserted(0);
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        private ImageView galleryImageView;
        private Context context;


        public ImageViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            galleryImageView = itemView.findViewById(R.id.gallery_image_view);
        }

        public void bindViewHolder(File image, int position) {
            galleryImageView.setOnClickListener(v -> {
                context.startActivity(PicturePagerActivity.newIntent(context, imagesPath, position),
                        ActivityOptionsCompat.makeScaleUpAnimation(
                                galleryImageView,
                                0,
                                0,
                                galleryImageView.getWidth(),
                                galleryImageView.getHeight()).toBundle()
                );
            });
            RequestOptions options = new RequestOptions();
            options = options.centerCrop();

            Glide.with(context)
                    .load(image)
                    .error(Glide.with(context).load(ContextCompat.getDrawable(context, R.drawable.ic_arrow_back)))
                    .apply(options)
                    .into(galleryImageView);
        }
    }
}
