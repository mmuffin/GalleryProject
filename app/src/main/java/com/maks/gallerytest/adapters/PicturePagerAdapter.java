package com.maks.gallerytest.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.maks.gallerytest.PictureFragment;

import java.util.List;

/**
 * Created by maks on 12/26/17.
 */

public class PicturePagerAdapter extends FragmentStatePagerAdapter {

    private List<String> imagesPath;

    public PicturePagerAdapter(FragmentManager fm, List<String> imagesPath) {
        super(fm);
        this.imagesPath = imagesPath;
    }

    @Override
    public Fragment getItem(int position) {
        return PictureFragment.newInstance(imagesPath.get(position));
    }

    @Override
    public int getCount() {
        return imagesPath.size();
    }
}
