package com.maks.gallerytest;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

import com.maks.gallerytest.adapters.GalleryImageAdapter;
import com.maks.gallerytest.network.ApiModule;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivityLogger";
    public static final int REQUEST_IMAGE_CAPTURE = 1;

    private RecyclerView galleryRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar();
        setupRecycler();
        /*ApiModule.getPixabaypiInterface()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(responseBody -> {
                    Log.d(TAG,responseBody.string());
                },throwable -> {
                    Log.d(TAG,Log.getStackTraceString(throwable));
                });*/
        ApiModule.getPixabaypiInterface()
                .getPixabayPics("flowers","photo")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull  Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        try {
                            Log.d(TAG,response.body().string());
                        } catch (NullPointerException | IOException e) {
                            Log.d(TAG,Log.getStackTraceString(e));
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call,@NonNull Throwable t) {
                        Log.d(TAG,Log.getStackTraceString(t));
                    }
                });

        /*for (String s : getAllGalleryImages()) {
            Log.d(TAG,s);
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupRecycler();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            galleryAddPic();
            return;
        }
        setupRecycler();
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setupActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //ActionBar actionBar = getSupportActionBar();
    }

    private void setupRecycler() {
        galleryRecyclerView = findViewById(R.id.galleryRecycler);
        galleryRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        GalleryImageAdapter adapter = new GalleryImageAdapter(getAllGalleryImages());
        galleryRecyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_make_photo:
                dispatchTakePictureIntent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<String> getAllGalleryImages() {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_size;
        ArrayList<String> listOfAllImages = new ArrayList<>();
        String absolutePathOfImage;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.MediaColumns.SIZE,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        cursor = this.getContentResolver().query(uri, projection, null,
                null, MediaStore.MediaColumns.DATE_MODIFIED + " DESC");

        column_index_size = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.SIZE);
        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        /*column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);*/
        while (cursor.moveToNext()) {
            if (cursor.getInt(column_index_size) == 0) continue;
            absolutePathOfImage = cursor.getString(column_index_data);

            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;
    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        File storageDir = Environment.getExternalStorageDirectory();
        File image = File.createTempFile(
                "example",  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        new Handler().postDelayed(() -> {
            ((GalleryImageAdapter) galleryRecyclerView.getAdapter()).addItem(mCurrentPhotoPath);
            galleryRecyclerView.scrollToPosition(0);
        }, 1000);
    }

}
