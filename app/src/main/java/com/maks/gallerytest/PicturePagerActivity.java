package com.maks.gallerytest;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.maks.gallerytest.adapters.PicturePagerAdapter;

import java.io.File;
import java.util.ArrayList;

public class PicturePagerActivity extends AppCompatActivity {

    private static final String EXTRA_IMAGES_PATH = "extra_image_paths";
    private static final String EXTRA_POSITION = "extra_images_path";

    public static Intent newIntent(Context context, ArrayList<String> imagesPath, int position) {
        Intent intent = new Intent(context, PicturePagerActivity.class);
        intent.putStringArrayListExtra(EXTRA_IMAGES_PATH, imagesPath);
        intent.putExtra(EXTRA_POSITION, position);
        return intent;
    }

    private ViewPager imageViewPager;
    private ArrayList<String> imagesPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);
        setupToolbar();
        setupViewPager();
        /*ImageView backButton = findViewById(R.id.icon_back);
        backButton.setOnClickListener(v -> onBackPressed());*/

       /* pictureImageView = findViewById(R.id.picture_image_view);
        if(getIntent() != null) {
            String imagePath = getIntent().getStringExtra(EXTRA_IMAGE_PATH);
            Picasso.with(this)
                    .load(new File(imagePath))
                    //.fit()
                    .into(pictureImageView);
        }*/

    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        }
    }

    public void changeAppBarVisibility() {
        AppBarLayout appBar = findViewById(R.id.app_bar);
        if (appBar.getVisibility() == View.VISIBLE) {
            appBar.setVisibility(View.GONE);
        } else {
            appBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                try {
                    String imagePath = imagesPath.get(imageViewPager.getCurrentItem());
                    File file = new File(imagePath);
                    Log.d("ExternalStorage", "Delete start!");
                    boolean delete = file.delete();
                    if (delete) {
                        finish();
                        //finishActivity(imageViewPager.getCurrentItem());
                        Toast.makeText(this, "Delete success", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Can`t delete", Toast.LENGTH_SHORT).show();
                    }
                    callBroadCast();
                    imagesPath.remove(imagePath);
                    imageViewPager.getAdapter().notifyDataSetChanged();
                    /*imageViewPager.setCurrentItem(imageViewPager.getCurrentItem() + 1);
                  */
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void callBroadCast() {
        Log.e("-->", " >= 14");
        /*
         *   (non-Javadoc)
         * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
         */
        MediaScannerConnection.scanFile(this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, (path, uri) -> {
            Log.e("ExternalStorage", "Scanned " + path + ":");
            Log.e("ExternalStorage", "-> uri=" + uri);
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.picture_pager_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void setTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    private void setupViewPager() {
        imageViewPager = findViewById(R.id.image_viewPager);
        Intent intent = getIntent();
        if (intent != null) {
            FragmentManager fm = getSupportFragmentManager();
            int position = intent.getIntExtra(EXTRA_POSITION, 0);
            imagesPath = intent.getStringArrayListExtra(EXTRA_IMAGES_PATH);
            imageViewPager.setAdapter(new PicturePagerAdapter(fm, imagesPath));
            setTitle(new File(imagesPath.get(position)).getName());
            imageViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    setTitle(new File(imagesPath.get(position)).getName());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            imageViewPager.setCurrentItem(position);
        }

    }


}
