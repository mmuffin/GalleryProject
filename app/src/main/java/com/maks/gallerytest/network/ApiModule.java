package com.maks.gallerytest.network;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by maks on 1/11/18.
 */

public class ApiModule {

    private static final String PIXABAY_API_KEY = "7668154-22523671f525f700d9fef6a16";
    private static final String PIXABAY_API_URL = "https://pixabay.com/api/";

    /**
     * @return OkHttpClient for work with CryptonatorApi
     */
    public static PixabayService getPixabaypiInterface() {

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.interceptors().add(chain -> {
            Request original = chain.request();
            HttpUrl url = original.url().newBuilder()
                    .addQueryParameter("key",PIXABAY_API_KEY).build();
            Request request = original.newBuilder()
                    .url(url)
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder().
                baseUrl(PIXABAY_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client.build());

        return builder.build().create(PixabayService.class);
    }
}
