package com.maks.gallerytest.network;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by maks on 1/11/18.
 */

public interface PixabayService {

    @GET("#")
    Call<ResponseBody> getPixabayPics(@Query("q") String query,
                                      @Query("image_type") String type);

}
